<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 11/10/18
 * Time: 5:48 PM
 */

namespace application\models;


use application\core\Db;

class Main
{
    public function __construct()
    {

    }

    public static function getAllNes(){
        $db = Db::getConnect();

        $result = $db->query('select news from news');


        $result->setFetchMode(\PDO::FETCH_ASSOC);

        $i = 0;
        while($row = $result->fetch()){
            $newsItem[$i]['news'] = $row['news'];
            $i++;
        }

        return $newsItem;
    }
}