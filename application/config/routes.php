<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 11/9/18
 * Time: 8:20 PM
 */

return [
    '' => [
        'controller' => 'main',
        'action' => 'index',
    ],

    'account/login' => [
        'controller' => 'account',
        'action' =>  'login'
    ],

    'account/register' => [
        'controller' => 'account',
        'action' =>  'register'
    ],

    'account/delete' => [
        'controller' => 'account',
        'action' =>  'delete'
    ],

];